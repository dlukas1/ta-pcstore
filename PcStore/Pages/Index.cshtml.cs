﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PcStore.Domain;

namespace PcStore.Pages
{
    public class IndexModel : PageModel
    {
        public string PriceSort { get; set; }
        public string PopularSort { get; set; }
        public string CurrentSort { get; set; }

        public IQueryable<Product> Products = DAL.GetProducts().AsQueryable();

        //docs.microsoft.com/en-us/aspnet/core/data/ef-rp/sort-filter-page?view=aspnetcore-2.1

        public void OnGet(string sortOrder)
        {
            CurrentSort = sortOrder;
            PriceSort = sortOrder == "price_asc" ? "price_desc" : "price_asc";
            PopularSort = sortOrder == "pop_asc" ? "pop_desc" : "pop_asc";

            switch (sortOrder)
            {
                case "price_desc":
                    Products = Products.OrderByDescending(p => p.Price);
                    break;
                case "price_asc":
                    Products = Products.OrderBy(p => p.Price);
                    break;
                case "pop_desc":
                    Products = Products.OrderByDescending(p => p.Popularity);
                    break;
                case "pop_asc":
                    Products = Products.OrderBy(p => p.Popularity);
                    break;
                default:
                    Products = Products.OrderBy(p => p.Id);
                    break;
            }
        }
    }
}
