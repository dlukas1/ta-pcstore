using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PcStore.Domain;

namespace PcStore.Pages
{
    public class DetailsModel : PageModel
    {
        public Product Product { get; set; }
        public IActionResult OnGet(string id)
        {
            Product = DAL.GetById(id);
            return Page();
        }
    }
}