﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PcStore.Domain
{
    public  class DAL
    {

        public static List<Product> GetProducts()
        {
            var productsOnly = new List<Product>();
            var details = new List<Product>();
            XDocument xDoc = XDocument.Load(@"Data\List.xml");
            XDocument xDoc1 = XDocument.Load(@"Data\Detail.xml");

            productsOnly = xDoc.Descendants("Product").Select(x => new Product()
            {
                Id = (string)x.Attribute("id"),
                Title = (string)x.Element("Title"),
                Description = (string)x.Element("Description"),
                ImagePath = (string)x.Element("Image"),
                Price = Decimal.Parse(x.Element("Price").Value),
                Popularity = Int32.Parse(x.Element("Sorting").Element("Popular").Value)
            }).ToList();


            details = xDoc1.Descendants("Product").Select(x => new Product()
            {
                Id = (string)x.Attribute("id"),
                Specifications = getSpecsToString(x.Element("Specs").Elements("Spec")),
                Availability = (string)x.Element("Availability")
            }).ToList();

            //joining productList + detailList
            var products = from p in productsOnly
                           join d in details on p.Id equals d.Id
                           select new Product
                           {
                               Id = p.Id,
                               Title = p.Title,
                               Description = p.Description,
                               ImagePath = p.ImagePath,
                               Price = p.Price,
                               Popularity = p.Popularity,
                               Specifications = d.Specifications,
                               Availability = d.Availability
                           };



            return products.ToList();
        }

        public static Product GetById(string id)
        {
            return GetProducts().Where(p => p.Id == id).SingleOrDefault();
        }
        private static string getSpecsToString(IEnumerable<XElement> specsList)
        {
            string allSpecs = "";
            foreach (var x in specsList)
            {
                allSpecs += x.Value + " ";
            }
            return allSpecs;
        }
    }
}
