﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PcStore.Domain
{
    public enum SortState
    {
        PriceAsc,
        PriceDesc,
        PopAsc,
        PopDesc
    }
}
